extends Node

enum Meses {
    ENE = 1,
    FEB = 2,
    MAR = 3,
    ABR = 4,
    MAY = 5,
    JUN = 6,
    JUL = 7,
    AGO = 8,
    SEP = 9,
    OCT = 10,
    NOV = 11,
    DIC = 12
}

var CantidadDiasMes = {
    1: 31,
    2: 29,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}

var AcumCantidadDiasMes = {
    0: 0,
    1: 31,
    2: 60,
    3: 91,
    4: 121,
    5: 152,
    6: 182,
    7: 213,
    8: 244,
    9: 274,
    10: 305,
    11: 335,
    12: 366
}

const HORAS_DIA = 24

func _ready():
    pass
