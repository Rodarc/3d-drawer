extends DibujadorPuntos

class_name DibujadorPuntosMultiMesh

# onready var multi_mesh_instance: MultiMeshInstance = get_node("DrawerMultMesh")
var multi_mesh_instance: MultiMeshInstance = null


var Posiciones: PoolVector3Array = PoolVector3Array()
var Radios: PoolRealArray = PoolRealArray()
var Colores: PoolColorArray = PoolColorArray()

func _ready():
    
    multi_mesh_instance = MultiMeshInstance.new()
    add_child(multi_mesh_instance)
    multi_mesh_instance.material_override = load("res://Materials/MaterialVertexColor.tres")
    create_template() # llamando a la funcion de la clase padre, no se si deberia estar ahi

    #create_points_default()
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_3D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE

    multimesh.mesh = template
    multimesh.instance_count = 0

    multi_mesh_instance.multimesh = multimesh
    pass

func create_points_default():
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_3D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
    
    multimesh.mesh = template
    multimesh.instance_count = 10000


    #var i = 0
    for i in multimesh.instance_count:
        var posicion = Vector3(rand_range(-9.60, 9.60), rand_range(-5.40, 5.40), rand_range(-9.60, 9.60))
        var radio = rand_range(0.05, 0.1)
        var new_transform: Transform = Transform(Vector3(1, 0, 0) * radio, Vector3(0, 1, 0) * radio, Vector3(0, 0, 1) * radio, posicion)
        multimesh.set_instance_transform(i, new_transform)
        #dibujador_puntos.AgregarPunto(posicion, radio, Color.black)
        #i += 1
    multi_mesh_instance.multimesh = multimesh


func set_new_multimesh():
    # crea un nuevo multimesh con la informacio actualizada de Posiciones y radios y colores
    # var old_multimesh = multi_mesh_instance.multimesh
    # esta actaulizacion es lo suficientemente rapida para agregar varios putnos de golpe, no para una actualizacion
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_3D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
    
    multimesh.mesh = template
    multimesh.instance_count = Posiciones.size()
    
    for i in multimesh.instance_count:
        var new_transform: Transform = Transform(Vector3(1, 0, 0) * Radios[i], Vector3(0, 1, 0) * Radios[i], Vector3(0, 0, 1) * Radios[i], Posiciones[i])
        multimesh.set_instance_transform(i, new_transform)
        multimesh.set_instance_color(i, Colores[i])
    
    multi_mesh_instance.multimesh = multimesh
    #old_multimesh.free() multi mesh is a resource, dont need a free()
    


func AgregarPunto(Posicion: Vector3, Radio: float, Coloracion: Color) -> int:
    var id = Posiciones.size()
    Posiciones.push_back(Posicion)
    Radios.push_back(Radio)
    Colores.push_back(Coloracion)

    set_new_multimesh()
    
    # probar con un multimesh duplicate
    
    # aumentando el instance count, no parece funcionar
#    var new_multimesh = multi_mesh_instance.multimesh.duplicate()
#    var id: int = new_multimesh.instance_count
#    new_multimesh.instance_count += 1
#    var new_transform: Transform = Transform(Vector3(1, 0, 0) * Radio, Vector3(0, 1, 0) * Radio, Vector3(0, 0, 1) * Radio, Posicion)
#    multi_mesh_instance.multimesh.set_instance_transform(id, new_transform)
#    multi_mesh_instance.multimesh.set_instance_color(id, Coloracion)
#    multi_mesh_instance.multimesh = new_multimesh
    return id

func AgregarPuntos(new_posiciones: PoolVector3Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    var init_range = Posiciones.size()
    Posiciones.append_array(new_posiciones)
    Radios.append_array(new_radios)
    Colores.append_array(new_colores)
    
    set_new_multimesh()
    return PoolIntArray(range(init_range, Posiciones.size()))
