extends MultiMeshInstance

var template: ArrayMesh

# El codigo aqui comentado, se queda como referencia, ya que fucniona tambien en la funcion ready de un MultiMeshInstance
# Por consistencia con las clases dibujador, la configuracion y manupulacion de este MultiMeshInstance se hará desde una clase dibujador

func _ready():
#    randomize()
#    create_piramide_template()
#
#    var new_multimesh = MultiMesh.new()
#    new_multimesh.transform_format = MultiMesh.TRANSFORM_3D
#    new_multimesh.color_format = MultiMesh.COLOR_NONE
#    new_multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
#    # como establezco el mesh a usar?
#    new_multimesh.mesh = template
#    new_multimesh.instance_count = 40000
#
#
#    # crear por ahora aqui lo aleatorio
#    for i in new_multimesh.instance_count:
#        var posicion = Vector3(rand_range(-9.60, 9.60), rand_range(-5.40, 5.40), rand_range(-9.60, 9.60))
#        var radio = rand_range(0.05, 0.1)
#        var new_transform: Transform = Transform(Vector3(1, 0, 0) * radio, Vector3(0, 1, 0) * radio, Vector3(0, 0, 1) * radio, posicion)
#
#        new_multimesh.set_instance_transform(i, new_transform)
#
#    multimesh = new_multimesh
    pass

func create_piramide_template():
    # solo por facilidad se ha copiado esta funcion aqui
    var radio: float = 1
    var numero_caras: int = 3
    var vertices: PoolVector3Array = PoolVector3Array()
    var normals: PoolVector3Array = PoolVector3Array()
    var colors: PoolColorArray = PoolColorArray()
    var i: int = 0
    var delta_angle: float = 2.0*PI/numero_caras
    while i < numero_caras: # base
        vertices.push_back(Vector3(radio * sin(i*delta_angle), 0, radio*cos(i*delta_angle)))
        normals.push_back(Vector3(0, 1, 0))# en reliadad deberia apuntar hacia abajo
        colors.push_back(Color.black)
        i += 1
    i = 0
    while i < numero_caras: # caras laterales
        vertices.push_back(Vector3(0, 1, 0))
        vertices.push_back(vertices[(i+1) % numero_caras])
        vertices.push_back(vertices[i])
        
        #calcular las normales correspondientes
        var normal = (vertices[i] - Vector3(0, 1, 0)).cross(vertices[(i+1) % numero_caras] - Vector3(0, 1, 0))
        normals.push_back(normal)
        normals.push_back(normal)
        normals.push_back(normal)
        colors.push_back(Color.black)
        colors.push_back(Color.black)
        colors.push_back(Color.black)
        i += 1
    
    # Initialize the ArrayMesh. esto es repetitivo, convertirlo en una funcion
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    arrays[ArrayMesh.ARRAY_NORMAL] = normals
    arrays[ArrayMesh.ARRAY_COLOR] = colors
    # en esta forma solo se ha usado el array de vertices, pero hay mas arrays
    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    template = arr_mesh
