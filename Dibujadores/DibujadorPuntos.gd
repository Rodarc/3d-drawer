extends Dibujador

class_name DibujadorPuntos

# dibujar varios MeshInstance, uno para cada punto
var template: ArrayMesh


func _ready():
    pass

func Mostrar():
    .Mostrar()
    print("DibujadorPuntos: mostrar")
    
func Ocultar():
    print("DibujadorPuntos: ocultar")

func AgregarPunto(Posicion: Vector3, Radio: float, Coloracion: Color) -> int:
    return -1

func AgregarPuntos(new_posiciones: PoolVector3Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    return PoolIntArray()

func create_template():
    #template = FiguraLibrary.basic_triangle_3d_template()
    #template = FiguraLibrary.cube_template()
    template = FiguraLibrary.piramid_template()
