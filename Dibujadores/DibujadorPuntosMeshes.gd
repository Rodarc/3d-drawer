extends DibujadorPuntos

class_name DibujadorPuntosMeshes

# dibujar varios MeshInstance, uno para cada punto

# array de nodos MeshInstance, cada uno es un punto visual
var PuntosMeshes: Array= []

func _ready():
    
    # create_triangle_array_mesh()
    
    # create_triangle_surface_tool()
    
    create_template()
    
    
    # create_point()
    
    # Using 
    pass

func create_triangle_mesh_data_tool():
    # la herramienta mesh data tool sirve para sacar la informacion de un mesh para poder editar y luego cargarla
    # esto podria ser util para hacer el update
    pass

func create_point():
    var m = MeshInstance.new()
    #m.set_name("Point")
    m.mesh = template
    add_child(m)
    return m

func AgregarPunto(Posicion: Vector3, Radio: float, Coloracion: Color) -> int:
    # el radio estara en centimetros
    var id: int = PuntosMeshes.size()
    #posiciones.push_back(Posicion)
    #radios.push_back(Radio)
    #colores.push_back(Coloracion)
    var PuntoInstance = create_point()
    PuntoInstance.set_name("Punto %d" % (id))
    PuntoInstance.translation = Posicion
    PuntoInstance.scale = Vector3(Radio, Radio, Radio)
    # me faltaria usar el color
    return id

func create_triangle_array_mesh():
    # Using ArrayMesh
    var vertices = PoolVector3Array()
    vertices.push_back(Vector3(0, 1, 0))
    vertices.push_back(Vector3(1, 0, 0))
    vertices.push_back(Vector3(0, 0, 1))
    # Initialize the ArrayMesh.
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    # en esta forma solo se ha usado el array de vertices, pero hay mas arrays
    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    var m = MeshInstance.new()
    m.set_name("Punto")
    m.mesh = arr_mesh
    add_child(m)
    pass

func create_triangle_surface_tool():
    # forma alternativa, solo cambia la forma en que se le pasa la información al componente, esta se parece a opengl
    var st = SurfaceTool.new()
    st.begin(Mesh.PRIMITIVE_TRIANGLES)
    st.add_color(Color(1, 0, 0))
    st.add_uv(Vector2(0, 0))
    st.add_vertex(Vector3(0, 1, 0))
    st.add_color(Color(1, 0, 0))
    st.add_uv(Vector2(0, 1))
    st.add_vertex(Vector3(1, 0, 0))
    st.add_color(Color(1, 0, 0))
    st.add_uv(Vector2(1, 1))
    st.add_vertex(Vector3(0, 0, 1))
    
    var arr_mesh: ArrayMesh = st.commit()
    var m = MeshInstance.new()
    m.set_name("Punto")
    m.mesh = arr_mesh
    add_child(m)
    
    pass
