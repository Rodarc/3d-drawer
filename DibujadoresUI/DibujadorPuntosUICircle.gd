extends DibujadorPuntosUI

class_name DibujadorPuntosUICircle

var Posiciones: PoolVector2Array = PoolVector2Array()
var Radios: PoolRealArray = PoolRealArray()
var Colores: PoolColorArray = PoolColorArray()


func _ready():
    pass

func _draw():
    var i: int = 0
    while i < Posiciones.size():
        # las posiciones de esta funcion draw circle son relativas al dibujador
        draw_circle(Posiciones[i], Radios[i], Colores[i])
        i += 1
    pass

# no es necesario hacer llamada de update todo los frames, solo cuando se haga algun cambio
#func _physics_process(delta):
    #update()


func AgregarPunto(Posicion: Vector2, Radio: float, Coloracion: Color) -> int:
    var id = Posiciones.size()
    Posiciones.push_back(Posicion)
    Radios.push_back(Radio)
    Colores.push_back(Coloracion)
    #mandar a hacer draw update
    update()
    return id


func AgregarPuntos(new_posiciones: PoolVector2Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    var init_range = Posiciones.size()
    Posiciones.append_array(new_posiciones)
    Radios.append_array(new_radios)
    Colores.append_array(new_colores)
    
    update()
    return PoolIntArray(range(init_range, Posiciones.size()))
