extends CanvasLayer

onready var fps_counter: Label = get_node("MarginContainer/VBoxContainer/FPSCounter")

func _ready():
    pass

func _process(delta):
    fps_counter.text = str(Performance.get_monitor(Performance.TIME_FPS))
