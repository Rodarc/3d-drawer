extends Control

onready var selector_variable: OptionButton = get_node("HBoxContainer/Panel/HBoxContainer/VBoxContainer/MarginContainer/HBoxContainer/VariablesOptionButton")
onready var selector_anio: OptionButton = get_node("HBoxContainer/Panel/HBoxContainer/VBoxContainer/MarginContainer4/HBoxContainer/AnioOptionButton")
onready var scatterplot: ScatterPlot = get_node("HBoxContainer/Panel/HBoxContainer/VisualizationContainer/ScatterPlot")
onready var visualizar_button: Button = get_node("HBoxContainer/Panel/HBoxContainer/VBoxContainer/MarginContainer5/VisualizarButton")

var load_data_thread: Thread = null

var variables = []
var years = []

var min_value
var max_value

var scatterplot_series = {}

var data = {}

func _ready():
    var files = DatasetLibrary.get_files_in_path(DatasetLibrary.extern_path)
    #print(files)
    variables = files
    var i: int = 0
    while i < variables.size():
        selector_variable.add_item(variables[i], i)
        i += 1
    
    pass

#esta clase se encargará de utilizar la libreria de bases de datos,  procesarla y pasarsela al escaterplot adecuadamente


func _on_VariablesOptionButton_item_selected(index):
    #inhabilitar el menu
    selector_anio.disabled = true
    visualizar_button.disabled = true
    
    load_data_thread = Thread.new()
    load_data_thread.start(self, "_load_data", index)
    
    
    pass

func _load_data(index):
    # debo leer los datos del archivo de esta variable
    
    #read
    var lines = DatasetLibrary.load_dataset_file(DatasetLibrary.extern_path + "/" + variables[selector_variable.get_item_id(index)])
    
    #process
    data.clear()
    min_value = INF
    max_value = -INF
    
    for line in lines:
        #if data.has(line["stationname"]):
        #    data[line["stationname"]].append(line)
        #else:
        #    data[line["stationname"]] = [line]
    # con esto la data ya esta ordenada por estaciones, pero no es manejable para mostrar, habira que serapar por años
        
        if not data.has(line["stationname"]):
            data[line["stationname"]] = {}
        # aqui ya siempre existe tal stationname
        var date_split = line["date"].split("-")
        var year = int(date_split[0])
        if not data[line["stationname"]].has(year):
            data[line["stationname"]][year] = []
        # aqui ya siempre existe tal año
        data[line["stationname"]][year].append(line)
        
        if line["conc"]:
            var val = float(line["conc"])
            if val < min_value:
                min_value = val
            if val > max_value:
                max_value = val
           

    
    print("all data loaded")
    print(data.keys()) # represan el nombre de las estaciones, no es igual en todas las variables, cambiar la interfaz de acuerdo a esto
    
    call_deferred("update_menu")
    
    # como he cambiado la variable debo mostrar los datos de esa variable
    # por lo tanto debo limpiar los datos del scatterplot y mandarle a plotear la data pertinente
    pass # Replace with function body.

func update_menu():
    #se entiende que data es listo
    years.clear()
    for key in data.keys():
        for year in data[key].keys():
            if not years.has(year):
                years.append(year)
        
        print(data[key].keys())
    years.sort()
    
    selector_anio.clear()
    var i: int = 0
    while i < years.size():
        selector_anio.add_item(str(years[i]), i)
        i += 1
    
    selector_anio.disabled = false
    visualizar_button.disabled = false
    load_data_thread.wait_to_finish()

func _on_AnioOptionButton_item_selected(index):
    pass # Replace with function body.


func _on_Button_pressed():
    # visualizar lo seleccionado
    # opcion 1: tener variables que al momento de cambiar el formulario se vayan actualizando
    # opcion 2: leer los valores directamente de los controles
    # ambos tiene complicaciones, como en este caso, puede que algun componente cargue informacion para los otros al momento de cambiar
    # por lo tanto podria asociar a su funcion de cambio el guardar informacion en algun lado, o tal vez no.
    
    scatterplot.clear()
    
    # obtener el valor maximo y minimo para el eje Y
    var horas: int = 366 * Date.HORAS_DIA
    var selected_anio = years[selector_anio.get_selected_id()]
    print("Año seleccionado", selected_anio)
    
    # configurar el scatterplot
    scatterplot.x_field = "date"
    scatterplot.y_field = "conc"
    scatterplot.set_range_x(0, horas)# en realidad deberia ser el mismo tipo de dato que el que esta en la data que va como sere al scatterplot, por facilidad le mandare la cantidad de horas de un año por ahora
    scatterplot.set_range_y(min_value, max_value)
    
    # pasara la data al scatterplot
    print(data.keys())
    for station in data.keys():
        print(station)
        #var id_serie = scatterplot.agregar_serie(data[station][years[selected_anio]])
        if data[station].has(selected_anio):
            var id_serie = scatterplot.agregar_serie(data[station][selected_anio])
            scatterplot_series[station] = id_serie
    
    # mandar a dibujar
    pass # Replace with function body.


func _on_ClearButton_pressed():
    scatterplot.clear()
    pass # Replace with function body.
