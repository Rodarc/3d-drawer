extends Node

export var numero_elementos: float = 100
onready var dibujador_puntos: DibujadorPuntos = get_node("MultiMesh")

func _ready():
    
    #prueba de lectura de archivos
    #var files = DatasetLibrary.get_files_in_path(DatasetLibrary.extern_path)
    #print(files)
    #var lines = DatasetLibrary.load_file_lines(DatasetLibrary.extern_path + "/" + files[0])
    
    #var lines = DatasetLibrary.load_file_csv(DatasetLibrary.extern_path + "/" + files[1])
    
    #var lines = DatasetLibrary.load_dataset_file(DatasetLibrary.extern_path + "/" + files[0])
    
    randomize()
    # GenerarPuntosAleatorios(numero_elementos)
    GenerarPuntosAleatorios_bulk(numero_elementos)
    pass

func _process(delta):
    if Input.is_action_just_pressed("ui_up"):
        var posicion = Vector3(rand_range(-9.60, 9.60), rand_range(-5.40, 5.40), rand_range(-9.60, 9.60))
        var radio = rand_range(0.05, 0.1)
        dibujador_puntos.AgregarPunto(posicion, radio, Color.black)

func GenerarPuntosAleatorios(num: int):
    var i = 0
    while i < num:
        var posicion = Vector3(rand_range(-9.60, 9.60), rand_range(-5.40, 5.40), rand_range(-9.60, 9.60))
        var radio = rand_range(0.05, 0.1)
        dibujador_puntos.AgregarPunto(posicion, radio, Color.black)
        i += 1

func GenerarPuntosAleatorios_bulk(num: int):
    var posiciones: PoolVector3Array = PoolVector3Array()
    var radios: PoolRealArray = PoolRealArray()
    var colores: PoolColorArray = PoolColorArray()
    var i = 0
    while i < num:
        posiciones.push_back(Vector3(rand_range(-9.60, 9.60), rand_range(-5.40, 5.40), rand_range(-9.60, 9.60)))
        radios.push_back(rand_range(0.05, 0.1))
        colores.push_back(Color.black)
        i += 1
    dibujador_puntos.AgregarPuntos(posiciones, radios, colores)
