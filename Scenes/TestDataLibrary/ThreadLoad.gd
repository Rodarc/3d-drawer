extends MarginContainer

onready var selector_file: OptionButton = get_node("VBoxContainer/OptionButton")
onready var status_label: Label = get_node("VBoxContainer/Label")
onready var load_button: Button = get_node("VBoxContainer/Button")

enum Estados {WAITING, LOADING, LOADED}

var labels = {Estados.WAITING: "Esperando", Estados.LOADING: "Cargando", Estados.LOADED: "Cargado"}


var files  = []
var read_thread
var data = {}

func _ready():
    files = DatasetLibrary.get_files_in_path(DatasetLibrary.extern_path)
    print(files)
    var i: int = 0
    while i < files.size():
        selector_file.add_item(files[i], i)
        i += 1
    status_label.text = labels[Estados.WAITING]


func _on_Button_pressed():
    load_button.disabled = true
    status_label.text = labels[Estados.LOADING]
    
    read_thread = Thread.new()
    read_thread.start(self, "_load_file_thread")
    pass # Replace with function body.

func _load_file_thread(userdata):
    data.clear()
    var lines = DatasetLibrary.load_dataset_file(DatasetLibrary.extern_path + "/" + files[selector_file.get_selected_id()])
    
    for line in lines:
        if not data.has(line["stationname"]):
            data[line["stationname"]] = []
        data[line["stationname"]].append(line)

    print("all data loaded")
    #status_label.text = labels[Estados.LOADED]
    call_deferred("show_data")#solucion 1 para comunicar que ya he terminado la carga
    # cuando use la libreria para esto debo llamar a una funcion que reciba esta data, un callback
    # y debo encontrar una forma de limpiar el thread

func show_data():#visualizar
    status_label.text = labels[Estados.LOADED]
    load_button.disabled = false
    read_thread.wait_to_finish()
    print(data.keys())

func _exit_tree():
    #read_thread.wait_to_finish()
    pass
