extends Node

export var numero_elementos: float = 100
onready var dibujador_puntos: DibujadorPuntosUI = get_node("CanvasLayer/Polygon")
#onready var dibujador_puntos: DibujadorPuntosUI = get_node("DibujadorPuntos")

func _ready():
    pass

    randomize()
    # GenerarPuntosAleatorios(numero_elementos)
    GenerarPuntosAleatorios_bulk(numero_elementos)
    pass

func _process(delta):
    if Input.is_action_just_pressed("ui_up"):
        var posicion = Vector2(rand_range(-960, 960), rand_range(-540, 540))
        var radio = rand_range(5, 10)
        dibujador_puntos.AgregarPunto(posicion, radio, Color.black)

func GenerarPuntosAleatorios(num: int):
    var i = 0
    while i < num:
        var posicion = Vector2(rand_range(-960, 960), rand_range(-540, 540))
        var radio = rand_range(5, 10)
        dibujador_puntos.AgregarPunto(posicion, radio, Color.black)
        i += 1

func GenerarPuntosAleatorios_bulk(num: int):
    var posiciones: PoolVector2Array = PoolVector2Array()
    var radios: PoolRealArray = PoolRealArray()
    var colores: PoolColorArray = PoolColorArray()
    var i = 0
    while i < num:
        posiciones.push_back(Vector2(rand_range(-960, 960), rand_range(-540, 540)))
        radios.push_back(rand_range(5, 10))
        colores.push_back(Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1), rand_range(0.5, 1)))
        i += 1
    dibujador_puntos.AgregarPuntos(posiciones, radios, colores)
