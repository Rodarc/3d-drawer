extends Control

class_name ScatterPlot

var dibujador_puntos: DibujadorPuntosUI = null
var margen: float = 10.0

var puntos_size: int = 5
var colores = [Color.blue, Color.red, Color.green, Color.yellow, Color.aqua, Color.darkgreen, Color.deepskyblue, Color.purple, Color.orange, Color.darkviolet]

var min_x
var max_x # podria ser cualquier tipo
var min_y
var max_y

var x_field: String = ""
var y_field: String = ""

var series: Array = []

func _ready():
    # crear un dibujador de puntos
    dibujador_puntos = DibujadorPuntosUIMultiMesh.new()
    add_child(dibujador_puntos)
    # expandimos respecto al padre, para ocupar el contenedor donde este, o lo que sea que sea el padre
    dibujador_puntos.set_anchor_and_margin(MARGIN_RIGHT, 1, 0)
    dibujador_puntos.set_anchor_and_margin(MARGIN_BOTTOM, 1, 0)


func clear():
    series.clear()
    dibujador_puntos.clear()
    pass

func set_range_x(new_min_x, new_max_x):
    min_x = new_min_x
    max_x = new_max_x

func set_range_y(new_min_y, new_max_y):
    min_y = new_min_y
    max_y = new_max_y

func agregar_serie(new_serie):
    # guardar la data
    var id_serie = series.size()
    series.append(new_serie)
        
    # procesarla para generar los puntos de dibujo
    # por ahora se supone que recibire series de un solo año, asi que saltare esa parte del calculo
    var puntos = convertir_serie_a_puntos_dibujo(series[id_serie])
    var radios = PoolRealArray()
    var colors = PoolColorArray()
    for p in puntos:
        radios.append(puntos_size)
        colors.append(colores[id_serie])
        
    # enviar a dibujar
    dibujar_puntos(puntos, radios, colors)
    return id_serie

func convertir_serie_a_puntos_dibujo(serie) -> PoolVector2Array:
    # deberia usar los campos especificados en x_field y y_field
    var puntos: PoolVector2Array = PoolVector2Array()
    for dato in serie:
        if dato[y_field]:
            var date_split = dato[x_field].split("-")
            var mes = int(date_split[1])
            var dia = int(date_split[2])
            var hora = int(dato["hour"]) # este cambio deberia estar combinado en date (y_field)
            var value = float(dato[y_field])
            var punto = Vector2((Date.AcumCantidadDiasMes[mes - 1] + (dia - 1)) * Date.HORAS_DIA + (hora - 1), value)
            # normalizar el punto usando los minimos y maximos
            var punto_dibujar = Vector2((punto.x - min_x)/(max_x - min_x), (punto.y - min_y)/(max_y - min_y))
            
            # ¿los puntos ya deben estar asociados al size, por que deberia ser independiente del espacio del dibujador?
            var rect = dibujador_puntos.get_rect().size
            puntos.append(Vector2(punto_dibujar.x * rect.x, (1 - punto_dibujar.y) * rect.y))
    return puntos

func dibujar_puntos(puntos: PoolVector2Array, radios: PoolRealArray, colores: PoolColorArray):
    print(puntos.size(), radios.size(), colores.size())
    # agregar los puntos al dibujador, quiza convertirlos o algo asi
    # al dibujador se le pasarn puntos de 0 a 1, para ambos ejes, el los dibujara proporcionalmente a sus dimensiones en pantalla
    # por lo tanto el que se encargar de determinar estos valores de 0 a q es esta clase scatterplot, en funcion de los rangos que recibió
    # a esta funcion se llamara por lo menos una vez por cada estacion

    return dibujador_puntos.AgregarPuntos(puntos, radios, colores)
    # debo retornar los ids de los objetos dibujados
    # quiza los dibujadores deban tener capas, para asi usar una para cada serie

#necesito dibujar texto en ciertos lugares, esto lo puedo hacer aqui directamente
#las lineas tambien las dibujare directamente

# el problema con este enfoque es que no siempre recibire el mismo tipo de dato, decir, un eje puede ser un entero, flotantes, fechas, etc
# otra opcion es recibir la data ya normaliza, dibujarla y listo, solo devolver una referencia o algo, el problema es que no tendría información para mostrar en las etiquetas
# quiza deberia recibir ademas del campo, que tipo de campo es para saber como procesarlo, esto con librerias que seguramente usaran otras visualizaciones
