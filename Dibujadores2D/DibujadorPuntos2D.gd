extends Dibujador2D

class_name DibujadorPuntos2D

func _ready():
    pass


func Mostrar():
    .Mostrar()
    print("DibujadorPuntos2D: mostrar")
    
func Ocultar():
    print("DibujadorPuntos2D: ocultar")

func AgregarPunto(Posicion: Vector2, Radio: float, Coloracion: Color) -> int:
    return -1

func AgregarPuntos(new_posiciones: PoolVector2Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    return PoolIntArray()
