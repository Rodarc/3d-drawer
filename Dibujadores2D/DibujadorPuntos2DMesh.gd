extends DibujadorPuntos2D

class_name DibujadorPuntos2DMesh

var numero_lados = 20
var template: ArrayMesh
export var texture: Texture = null

var Posiciones: PoolVector2Array = PoolVector2Array()
var Radios: PoolRealArray = PoolRealArray()
var Colores: PoolColorArray = PoolColorArray()

func _ready():
    create_template()


func _draw():
    var i: int = 0
    while i < Posiciones.size():
        # las posiciones de esta funcion draw circle son relativas al dibujador
        var new_transform: Transform2D = Transform2D(Vector2(1, 0) * Radios[i], Vector2(0, 1) * Radios[i], Posiciones[i])
        draw_mesh(template, null, null, new_transform, Colores[i]) # con modulate puedo darle color a toda la figura
        i += 1
    pass

func create_template(): # ejemplo basico de un triangulo
    # template = FiguraLibrary.basic_triangle_template()
    template = FiguraLibrary.regular_polygon_mesh_2d_template(numero_lados)


func AgregarPunto(Posicion: Vector2, Radio: float, Coloracion: Color) -> int:
    var id = Posiciones.size()
    Posiciones.push_back(Posicion)
    Radios.push_back(Radio)
    Colores.push_back(Coloracion)
    #mandar a hacer draw update
    update()
    return id


func AgregarPuntos(new_posiciones: PoolVector2Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    var init_range = Posiciones.size()
    Posiciones.append_array(new_posiciones)
    Radios.append_array(new_radios)
    Colores.append_array(new_colores)
    
    update()
    return PoolIntArray(range(init_range, Posiciones.size()))
