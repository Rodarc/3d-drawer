extends DibujadorPuntos2D

class_name DibujadorPuntos2DMultiMesh

var multi_mesh_instance: MultiMeshInstance2D = null
var numero_lados = 20
var template: ArrayMesh
var multimesh_draw: MultiMesh = null

var Posiciones: PoolVector2Array = PoolVector2Array()
var Radios: PoolRealArray = PoolRealArray()
var Colores: PoolColorArray = PoolColorArray()

func _ready():
    create_template()
    
    clear_multimesh()
    
    #create_default_multimesh_node()
    #crear nodo multimeshinstance2D
    

func _draw():
    draw_multimesh(multimesh_draw, null, null)

func create_template():
    template = FiguraLibrary.regular_polygon_mesh_2d_template(numero_lados)

func create_default_multimesh_node():
    # Si usara un nodo para dibujar
    # crear nodo multimeshinstance2D
    multi_mesh_instance = MultiMeshInstance2D.new()
    add_child(multi_mesh_instance)
    
    #crear multimesh
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_2D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE

    multimesh.mesh = template
    multimesh.instance_count = 0

    multi_mesh_instance.multimesh = multimesh

func clear_multimesh():
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_2D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE

    multimesh.mesh = template
    multimesh.instance_count = 0

    multimesh_draw = multimesh


func set_new_multimesh():
    # crea un nuevo multimesh con la informacio actualizada de Posiciones y radios y colores
    # var old_multimesh = multi_mesh_instance.multimesh
    # esta actaulizacion es lo suficientemente rapida para agregar varios putnos de golpe, no para una actualizacion
    var multimesh = MultiMesh.new()
    multimesh.transform_format = MultiMesh.TRANSFORM_2D
    multimesh.color_format = MultiMesh.COLOR_FLOAT
    multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
    
    multimesh.mesh = template
    multimesh.instance_count = Posiciones.size()
    
    for i in multimesh.instance_count:
        var new_transform: Transform2D = Transform2D(Vector2(1, 0) * Radios[i], Vector2(0, 1) * Radios[i], Posiciones[i])
        multimesh.set_instance_transform_2d(i, new_transform)
        multimesh.set_instance_color(i, Colores[i])
    
    #multi_mesh_instance.multimesh = multimesh
    #multi_mesh_instance.set_multimesh(multimesh)# para usar el nodo creado
    multimesh_draw = multimesh # para usar drawmesh
    

func AgregarPunto(Posicion: Vector2, Radio: float, Coloracion: Color) -> int:
    var id = Posiciones.size()
    Posiciones.push_back(Posicion)
    Radios.push_back(Radio)
    Colores.push_back(Coloracion)
    #mandar a hacer draw update
    set_new_multimesh()
    update() # para cuando se usa draw en lugar del nodo creado
    return id


func AgregarPuntos(new_posiciones: PoolVector2Array, new_radios: PoolRealArray, new_colores: PoolColorArray) -> PoolIntArray:
    var init_range = Posiciones.size()
    Posiciones.append_array(new_posiciones)
    Radios.append_array(new_radios)
    Colores.append_array(new_colores)
    
    set_new_multimesh()
    update() # para uando se usa draw en lugar del nodo cread
    return PoolIntArray(range(init_range, Posiciones.size()))
