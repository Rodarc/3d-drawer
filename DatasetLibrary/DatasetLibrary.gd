extends Node

var default_path: String = "res://Datasets"
var extern_path: String  = "D:/CalidadAire/Bases de Datos/DataAirQuality"


static func get_files_in_path(path: String):
    var files = []
    var dir = Directory.new()
    
    if dir.open(path) == OK:
        dir.list_dir_begin(true, true)
        var file = dir.get_next()
        while file: # != ""
            if not dir.current_is_dir():
                files.append(file)
            file = dir.get_next()
    
        dir.list_dir_end()
    return files


static func load_file(filename: String):
    # filename: path + filename
    # devuleve el contenido como un string

    var file = File.new()
    file.open(filename, File.READ)
    var content = file.get_as_text()
    file.close()
    return content


static func load_file_lines(filename: String):
    # filename: path + filename
    # lee linea por linea, devuelve un array de lineas
    
    var lines: PoolStringArray = PoolStringArray()
    var file = File.new()
    file.open(filename, File.READ)
    
    while not file.eof_reached():
        lines.append(file.get_line())
    
    file.close()
    return lines
    

static func load_file_csv(filename: String):
    # devuelve un array de contenidos
    # los archivos de la base de datos tiene la ultima linea en blanco, generando errores, limpiar archivos
    var lines: Array = []
    var file = File.new()
    file.open(filename, File.READ)
    
    while not file.eof_reached():
        var line = file.get_csv_line()
        if line.size(): # no esta funcionando, las nuevas lineas las retorna con tamaño 1
            lines.append(line)
    
    file.close()
    return lines


static func load_dataset_file(filename: String):
    # Retornar cada linea como una clase o como un diccionario con su correspondiente clave
    # debido a que la aplicacion sera personalizable, creo que lo mejor seria que todo sea un diccionario
    # para que en sea fácil seleccionar un atributo de los datos, aun si usamos distintas bases de datos, etc
    # asi tambien sera mas fácil manejar los resultados de todos los algoritmos
    # la primera linea del archivo contiene los nombres de las columnas, estas seran las claves
    # var lines = load_file_csv(filename) # esto excede algun limite de pool arrays, no usar
    var lines: PoolStringArray = load_file_lines(filename)
    var res = []
    if lines.size():
        var keys: PoolStringArray = lines[0].split(",")
        var i: int = 1
        while i < lines.size():
            var values: PoolStringArray = lines[i].split(",")
            var row = {}
            var j: int = 0
            while j < keys.size():
                #if values.size() < keys.size():# esto esta mal planteado
                if j >= values.size():
                    row[keys[j]] = null
                else:
                    row[keys[j]] = values[j] # podria fallar si hay algun dato vacio
                j += 1
            res.append(row)
            i += 1
    return res
