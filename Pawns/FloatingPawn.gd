extends Camera

export var Velocidad: float = 5

func _ready():
    #
    pass

func _physics_process(delta):
    
    # Movimiento del personaje
    var move_to_forward: float = Input.get_action_strength("move_forward")
    var move_to_backward: float = Input.get_action_strength("move_backward")
    var move_to_right: float = Input.get_action_strength("move_right")
    var move_to_left: float = Input.get_action_strength("move_left")
    var move_to_up: float = Input.get_action_strength("move_up")
    var move_to_down: float = Input.get_action_strength("move_down")
    
    var move_direction_x: float = 0.0
    if move_to_forward > move_to_backward:
        move_direction_x = move_to_forward
    elif move_to_forward < move_to_backward:
        move_direction_x = -move_to_backward
    
    var move_direction_z: float = 0.0
    if move_to_right > move_to_left:
        move_direction_z = move_to_right
    elif move_to_right < move_to_left:
        move_direction_z = -move_to_left
    
    var move_direction_y: float = 0.0
    if move_to_up > move_to_down:
        move_direction_y = move_to_up
    elif move_to_up < move_to_down:
        move_direction_y = -move_to_down
    
    var forward_vector: Vector3 = -transform.basis.z
    var right_vector: Vector3 = transform.basis.x
    var up_vector: Vector3 = transform.basis.y
    
    var desplazamiento: Vector3 = forward_vector * move_direction_x + right_vector * move_direction_z + up_vector * move_direction_y
    desplazamiento = desplazamiento.normalized()
    
    var movimiento: Vector3 = desplazamiento * Velocidad
    
    # calculando la nueva posicion
    var new_position: Vector3 = translation + movimiento * delta # el delta es para multiplicar la velocidad
    translation = new_position # forma 1, con los calculos anteriores
    # translate(movimiento) # forma 2
    


func _input(event):
    # Rotacion del personaje
    if event is InputEventMouseMotion:
        # con event.relative, se optiene la cantidad de desplazamiento de ese evento o frame
        # rotacion del personaje
        var rotation_y = -event.relative.x * 0.001 # este valor es una velocidad que se palica a cada frame, asumiendo que los frames sean constantes, deberia pulirse mejor esto
        rotate(Vector3.UP, rotation_y)
        orthonormalize()
        
        # rotacion de la camara
        var rotation_z = -event.relative.y * 0.001
        rotation.x = clamp(rotation.x + rotation_z, deg2rad(-170.0), deg2rad(170.0))
