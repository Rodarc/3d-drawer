extends Node


static func regular_polygon_2d_template(numero_lados: int) -> PoolVector2Array:
    var vertices = PoolVector2Array()
    #vertices.push_back(Vector2(1, 1))
    #vertices.push_back(Vector2(1, -1))
    #vertices.push_back(Vector2(-1, -1))
    
    #vertices.push_back(Vector2(-1, 1))
    #vertices.push_back(Vector2(1, 1))
    
    # el vertice con angulo 0 esta en direccion al eje x positivo
    var delta_angle: float = 2 * PI / numero_lados
    var i: int = 0
    while i < numero_lados:
        vertices.append(Vector2(cos(i * delta_angle), -sin(i * delta_angle)))
        i += 1
    
    return vertices


static func basic_triangle_2d_template() -> ArrayMesh: # ejemplo basico de un triangulo
    var vertices: PoolVector2Array = PoolVector2Array()
    vertices.push_back(Vector2(1, 1))
    vertices.push_back(Vector2(1, -1))
    vertices.push_back(Vector2(-1, -1))
    
    #var colores: PoolColorArray = PoolColorArray()
    #colores.append(Color(0.5, 0.5, 0.5, 1))
    #colores.append(Color(0.5, 0.5, 0.5, 1))
    #colores.append(Color(0.5, 0.5, 0.5, 1))
    
    # Initialize the ArrayMesh.
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    #arrays[ArrayMesh.ARRAY_COLOR] = colores# usando este array puedo darle color vertice por vertice, siempre que no haya el modulate, pero asi ya no se usaria como template, tendria que construir constantemente
    #tener cuidado si luego quiero usar shaders para algun efecto mas interesante
    # en esta forma solo se ha usado el array de vertices, pero hay mas arrays
    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    return arr_mesh

static func regular_polygon_mesh_2d_template(numero_lados: int) -> ArrayMesh:
    var vertices_polygon: PoolVector2Array = regular_polygon_2d_template(numero_lados)
    var vertices: PoolVector2Array = PoolVector2Array()
    
    var i: int = 0
    while i < vertices_polygon.size():
        vertices.append(vertices_polygon[(i + 1) % vertices_polygon.size()])
        vertices.append(vertices_polygon[(i) % vertices_polygon.size()])
        vertices.append(Vector2())
        i += 1
    
    # Initialize the ArrayMesh.
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices

    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    return arr_mesh

static func basic_triangle_3d_template():
    var vertices = PoolVector3Array()
    vertices.push_back(Vector3(0, 1, 0))
    vertices.push_back(Vector3(1, 0, 0))
    vertices.push_back(Vector3(0, 0, 1))
    
    # Initialize the ArrayMesh.
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    # en esta forma solo se ha usado el array de vertices, pero hay mas arrays
    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    return arr_mesh

static func cube_template():
    var vertices = PoolVector3Array()
    vertices.push_back(Vector3(1, 1, 1))
    vertices.push_back(Vector3(1, -1, 1))
    vertices.push_back(Vector3(-1, -1, 1))
    vertices.push_back(Vector3(1, 1, 1))
    vertices.push_back(Vector3(-1, -1, 1))
    vertices.push_back(Vector3(-1, 1, 1))
    
    var normals = PoolVector3Array()
    normals.push_back(Vector3(1, 1, 1))
    normals.push_back(Vector3(1, 1, 1))
    normals.push_back(Vector3(1, 1, 1))
    normals.push_back(Vector3(1, 1, 1))
    normals.push_back(Vector3(1, 1, 1))
    normals.push_back(Vector3(1, 1, 1))
    
    var colors = PoolColorArray()
    colors.push_back(Color.blue)
    colors.push_back(Color.blue)
    colors.push_back(Color.blue)
    colors.push_back(Color.red)
    colors.push_back(Color.red)
    colors.push_back(Color.red)
    # Initialize the ArrayMesh.
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    arrays[ArrayMesh.ARRAY_NORMAL] = normals
    arrays[ArrayMesh.ARRAY_COLOR] = colors

    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    return arr_mesh

func piramid_template():
    var radio: float = 1
    var numero_caras: int = 3
    var vertices: PoolVector3Array = PoolVector3Array()
    var normals: PoolVector3Array = PoolVector3Array()
    var colors: PoolColorArray = PoolColorArray()
    var i: int = 0
    var delta_angle: float = 2.0*PI/numero_caras
    while i < numero_caras: # base
        vertices.push_back(Vector3(radio * sin(i*delta_angle), 0, radio*cos(i*delta_angle)))
        normals.push_back(Vector3(0, 1, 0))# en reliadad deberia apuntar hacia abajo
        colors.push_back(Color.black)
        i += 1
    i = 0
    while i < numero_caras: # caras laterales
        vertices.push_back(Vector3(0, 1, 0))
        vertices.push_back(vertices[(i+1) % numero_caras])
        vertices.push_back(vertices[i])
        
        #calcular las normales correspondientes
        var normal = (vertices[i] - Vector3(0, 1, 0)).cross(vertices[(i+1) % numero_caras] - Vector3(0, 1, 0))
        normals.push_back(normal)
        normals.push_back(normal)
        normals.push_back(normal)
        colors.push_back(Color.black)
        colors.push_back(Color.black)
        colors.push_back(Color.black)
        i += 1
    
    # Initialize the ArrayMesh. esto es repetitivo, convertirlo en una funcion
    var arr_mesh = ArrayMesh.new()
    var arrays = []
    arrays.resize(ArrayMesh.ARRAY_MAX)
    arrays[ArrayMesh.ARRAY_VERTEX] = vertices
    arrays[ArrayMesh.ARRAY_NORMAL] = normals
    arrays[ArrayMesh.ARRAY_COLOR] = colors

    # Create the Mesh.
    arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
    return arr_mesh
